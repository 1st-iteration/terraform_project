terraform {
  required_providers {
    libvirt = {
      source  = "dmacvicar/libvirt"
      version = "~> 0.6.3"
    }
  }
}

provider "libvirt" {
  uri = "qemu:///system"
}

resource "null_resource" "create_storage_pool" {
  provisioner "local-exec" {
    command = <<EOT
if ! virsh pool-info default >/dev/null 2>&1; then
  virsh pool-define-as --name default --type dir --target /home/justine.deschamps@Digital-Grenoble.local/Documents/terraform_project
  virsh pool-start default
  virsh pool-autostart default
fi
EOT
  }
}

resource "libvirt_volume" "disk01" {
  name   = "debian12.img"
  pool   = "default"
  source = "./debian-12-generic-arm64-20240702-1796.qcow2"
  format = "qcow2"

  depends_on = [null_resource.create_storage_pool]
}

resource "libvirt_domain" "VM01" {
  name   = "domain_01"
  memory = 2048
  vcpu   = 2

  disk {
    volume_id = libvirt_volume.disk01.id
  }

  network_interface {
    network_name = "default"
  }

  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  graphics {
    type = "spice"
  }

  depends_on = [libvirt_volume.disk01]
}